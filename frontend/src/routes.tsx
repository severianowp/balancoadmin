import { Login } from "./pages/autenticacao/login";
import { Dashboard } from "./pages/dashboard";
import { Registros } from "./pages/registros";
import { Switch, Route, useHistory, Redirect } from "react-router-dom";
import React, { useContext } from "react";
import { Layout } from "./layout";
import { Cadastro } from "./pages/autenticacao/cadastro";
import { RotaPrivada } from "./auth/RotaPrivada";
import { AuthContext } from "./provider/authContex";

export const Routes = () => {
    const { logado } = useContext(AuthContext); // O Contexto irá controlar a sessão de login
    const history = useHistory();
    /**
     * Variável 'autenticado' irá mudar se o contexto for preenchido
     * por informações da API
     * Enquanto não houver informações, o usuário será redirecionado
     * para o login
     */

    let autenticado = logado; // variável criada para melhorar performance

    return (
        <div >
            <Switch>
                <Route path="/" exact component={() => <Redirect to="/login" />} />
                <Route path="/login" component={Login} />
                <Route path="/cadastro" component={Cadastro} />
                <Layout>
                    <RotaPrivada path="/dashboard" component={Dashboard} autenticado={autenticado} />
                    <RotaPrivada path="/registros" component={Registros} autenticado={autenticado} />
                </Layout>
                <Route component={() => <h1>Página 404</h1>} />
            </Switch>
        </div>
    )
}
