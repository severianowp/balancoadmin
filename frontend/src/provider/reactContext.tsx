import React, { Dispatch, SetStateAction, useState } from "react";

interface produtosTypes {

    nome: string,
    quantidade: number,
    valor: number,
    data: string,
}
interface usuarioTypes {
    nome: string;
    email: string,
    produtos: Array<produtosTypes>
}
interface stateTypes {
    usuario: usuarioTypes,
    setUsuario: Dispatch<SetStateAction<usuarioTypes>>
}

export const ReactContext = React.createContext<stateTypes>({
    usuario: {
        nome: "",
        email: '',
        produtos: [
            {
                nome: '',
                quantidade: 0,
                valor: 0,
                data: '',
            }
        ]
    },
    setUsuario: () => { },
});

export const ReactProvider: React.FC = (props) => {
    const [usuario, setUsuario] = useState({
        nome: '',
        email: '',
        produtos: [
            {
                nome: '',
                quantidade: 0,
                valor: 0,
                data: '',
            }
        ]
    });


    return (
        <ReactContext.Provider value={{ usuario, setUsuario }}>
            {props.children}
        </ReactContext.Provider>
    )
}