import React, { Dispatch, SetStateAction, useState } from "react";

interface authTypes {
    logado: boolean,
    setLogin: Dispatch<SetStateAction<boolean>>
}


export const AuthContext = React.createContext<authTypes>({
    logado: false,
    setLogin: () => { }
});

export const AuthProvider: React.FC = (props) => {
    const [logado, setLogin] = useState<boolean>(false);


    return (
        <AuthContext.Provider value={{ logado, setLogin }}>
            {props.children}
        </AuthContext.Provider>
    )
}