import React, { useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faUserCircle } from "@fortawesome/free-solid-svg-icons"
import { Link, useHistory } from "react-router-dom";
import { CadastroPost } from "../../api/autenticacao/cadastro";

export const Cadastro: React.FC = () => {

    /**
     * Estados
     */

    const [nome, setNome] = useState('');
    const [email, setEmail] = useState('');
    const [senha, setSenha] = useState('');
    const [divErro, mostrarErro] = useState('hidden');
    const [MensagemErro, setMensagem] = useState("teste");
    const [botaoPressionado, pressionarBotao] = useState(false);

    const history = useHistory();

    const regexp = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/i;

    const emailValido = regexp.test(email);

    async function enviar() {
        if (emailValido) {
            pressionarBotao(true)
            const cadastrar = await CadastroPost({
                nome: nome,
                email: email,
                senha: senha,
            })
            if (cadastrar.status === 201) {
                pressionarBotao(false)
                history.push("/login")
            } else {
                pressionarBotao(false)
                mostrarErro('block')
                setMensagem(cadastrar.message);

                setTimeout(() => mostrarErro('hidden'), 3000)
            }
        }
        else {
            mostrarErro('block')
            setMensagem("Email inválido");

            setTimeout(() => mostrarErro('hidden'), 3000)
        }
    }
    const teclaPressionada = (e: string) => {
        if (e === "Enter") {
            enviar();
        }
    }

    return (
        <div className="login h-3/6 xl:w-5-12 lg:w-3/12 md:w-7/12 min-w-500 mx-auto lg:mt-36 md:mt-2">
            <div className="login-titulo text-center text-4xl font-medium">
                Cadastro
                </div>
            <div className="login-box lg:mt-10 md:mt-2 py-6 shadow-lg bg-lightgray border border-gray-600 rounded-lg space-y-6">
                <div className={`mensagemErro w-10/12 mx-auto bg-red-600 rounded-md text-center text-lg font-medium text-white leading-10 ` + divErro}>
                    {MensagemErro}

                </div>
                <div className="w-32 h-32 mx-auto">
                    <FontAwesomeIcon icon={faUserCircle} size="8x" color="#113353" />
                </div>
                <div className="nome-input text-center">
                    <label className="block w-32">Nome:</label>
                    <input type="text" name="nome" onChange={event => setNome(event.target.value)} className="w-9/12 border border-gray-600 rounded-md pl-4 text-lg leading-10" />
                </div>
                <div className="email-input text-center">
                    <label className="block w-32">Email:</label>
                    <input type="text" name="email" onChange={event => setEmail(event.target.value)} onKeyUp={e => teclaPressionada(e.key)} className="w-9/12 border border-gray-600 rounded-md pl-4 text-lg leading-10" />
                </div>
                <div className="senha-input text-center">
                    <label className="block w-32">Senha:</label>
                    <input type="password" name="senha" onChange={event => setSenha(event.target.value)} onKeyUp={e => teclaPressionada(e.key)} className="w-9/12 border border-gray-600 mx-auto rounded-md pl-4 text-lg leading-10" />
                </div>
                {/* 
                    Condicional de renderização:
                    Botão é desabilitado para evitar
                    erros de multiplas requisições
                */}
                {botaoPressionado ?
                    <div className="botao mx-40 outline-none">
                        <div className="bg-gray-200 py-2 px-3 text-center rounded-md font-medium select-none border border-gray-600">
                            Cadastrando...</div>
                    </div>
                    :
                    <div className="botao mx-40 outline-none" onClick={enviar}>
                        <div className="bg-primary text-white py-2 px-3 text-center rounded-md font-medium select-none cursor-pointer">
                            CADASTRAR</div>
                    </div>
                }

                <div className="cadastrar text-xl text-center text-primary font-medium select-none cursor-pointer">
                    <Link to="/login"><span>Já tenho conta</span></Link>

                </div>
            </div>
        </div>
    )
}