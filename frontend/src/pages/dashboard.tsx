import React from "react";
import { Bloco } from "../components/compartilhados/bloco";

export const Dashboard: React.FC = () => {
    return (
        <div className="px-20 mt-16">

            <div className="conteudo-dashboard flex flex-wrap -mx-4 ">

                <div className="my-4 px-4 w-5/12 overflow-hidden h-60">
                    <Bloco texto={'Gasto em Produtos'}>
                    </Bloco>
                </div>
                <div className="my-4 px-4 w-5/12 overflow-hidden h-60">
                    <Bloco texto={'Vendas'}>
                    </Bloco>
                </div>
                <div className="my-4 px-4 w-5/12 overflow-hidden h-60">
                    <Bloco texto={'Gasto com Impostos'}>
                    </Bloco>
                </div>
                <div className="my-4 px-4 w-5/12 overflow-hidden h-60">
                    <Bloco texto={'Lucro'}>
                    </Bloco>
                </div>
            </div>
        </div>
    )
}