import React, { useContext } from "react";
import "../assets/css/registros/index.scss";
import { faEdit, faSearch, faTrash } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { BotaoAdicionar } from "../components/botaoAdicionar";
import { ReactContext } from "../provider/reactContext";

export const Registros: React.FC = () => {
    const { usuario } = useContext(ReactContext);
    const Produtos = usuario.produtos;

    console.log(Produtos);
    return (
        <div className="container-registros w-full h-full">
            <div className="box-registros w-11/12 shadow-lg mx-auto mt-20 py-8 px-4 rounded-md border border-gray-300">
                <div className="header flex">
                    <BotaoAdicionar />
                    <div className="filtro flex w-3/12 align-items-center">
                        <input type="text" className="w-full h-8 outline-none border-b border-gray-600 bg-transparent" />
                        <FontAwesomeIcon icon={faSearch} color="#113353" />
                    </div>
                </div>


                <div className="lista mt-8">
                    <div className="lista-titulos flex space-x-56 text-md font-medium pb-2 border-b border-gray-400">
                        <span className="ml-4">Nome</span>
                        <span>Quantidade</span>
                        <span>Valor (Unitário / Lote)</span>
                        <span>Data</span>
                        <span>Editar / Remover</span>

                    </div>
                    {Produtos.map((item, index) =>
                        <ul key={index} className="mt-2 divide-y divide-gray-600 bg-gray-100">
                            <li className="flex py-4 ml-2">
                                <span className="w-2/12">{item.nome}</span>
                                <span className="w-10 ml-20">{item.quantidade}</span>
                                <span className="w-2/12 ml-60">R$ {item.valor} / R$ {item.quantidade * item.valor}</span>
                                <span className="w-20 ml-36">{item.data}</span>
                                <div className="icones w-1/12 ml-48 space-x-8">
                                    <FontAwesomeIcon icon={faEdit} size="2x" color="#113353" />
                                    <FontAwesomeIcon icon={faTrash} size="2x" color="#113353" />
                                </div>
                            </li>
                        </ul>
                    )}
                </div>
            </div>
        </div>
    )
}