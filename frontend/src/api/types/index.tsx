export interface cadastroTypes {
    nome: string,
    email: string,
    senha: string
}

export interface loginTypes {
    email: string,
    senha: string
}