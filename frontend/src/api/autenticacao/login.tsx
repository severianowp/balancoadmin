import { Api } from "..";
import { loginTypes } from "../types";

export async function LoginGet({ email, senha }: loginTypes) {

    try {
        // todo metodo get aceita params como config , no back se usa o request.query
        const res = await Api.get("login", {
            params: {
                email: email,
                senha: senha
            }
        })
        if (res && res.data) {
            return res.data
        }
        return res
    } catch (err) {
        console.log(err)
    }

}