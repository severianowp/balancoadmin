import { Api } from "..";
import { Cadastro } from "../../pages/autenticacao/cadastro";
import { cadastroTypes } from "../types";

export async function CadastroPost({ nome, email, senha }: cadastroTypes) {

    try {
        const cadastrar = await Api.post("cadastro",
            { params: { nome: nome, email: email, senha: senha } });

        if (cadastrar.data.status === 201) {
        }

        return cadastrar.data;
    } catch (error) {
        console.log(error.status);

    }

}