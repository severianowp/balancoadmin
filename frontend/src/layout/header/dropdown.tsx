import { useHistory } from "react-router-dom";

export const Dropdown: React.FC = (props) => {

    const history = useHistory();
    const logout = () => {
        localStorage.setItem('autenticado', 'false');
        history.push("/login")
    }
    return (
        <div className="dropdown z-50 w-44 bg-white shadow-md absolute right-9 top-14 rounded-md text-center table select-none">
            <ul className="mt-4">
                <div className="nome w-8/12 mx-auto mb-4 py-1">
                    <li>{props.children}</li>

                </div>
                <hr className="w-11/12 mx-auto" />
                <div onClick={logout} className="logout w-8/12 text-center mx-auto my-4 py-1 cursor-pointer hover:bg-red-100 rounded-lg">

                    <li>Log Out</li>
                </div>
            </ul>
        </div>
    )
}