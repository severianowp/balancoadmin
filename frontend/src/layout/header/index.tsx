import React, { useContext } from "react";
import "../../assets/css/layout/header.scss";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUser } from "@fortawesome/free-solid-svg-icons";
import { Dropdown } from "./dropdown";
import { ReactContext } from "../../provider/reactContext";
import { useHistory } from "react-router-dom";
import { AuthContext } from "../../provider/authContex";

export const Header: React.FC = () => {
    let [mostrar, estadoDropdown] = React.useState<boolean>(false);
    /**
     * Contextos
     */
    const { usuario } = useContext(ReactContext);
    let infos = usuario;

    return (
        <div>
            <div className="navbar bg-white flex w-full h-20 px-10 items-center fixed">
                <div className="logo w-16 h-5/6 bg-primary rounded-full text-center table select-none">
                    <span className="align-middle table-cell text-white font-bold text-lg">
                        Logo
                    </span>
                </div>
                <div onClick={() => estadoDropdown(mostrar = !mostrar)}
                    className="usuario text-secondary font-medium text-lg space-x-2 cursor-pointer select-none">
                    <FontAwesomeIcon icon={faUser} />
                    <span>

                        {infos.email};
                </span>
                </div>
            </div>
            {mostrar ? <div>
                <Dropdown>{infos.nome}</Dropdown>
            </div> : ''}
        </div>
    )
}