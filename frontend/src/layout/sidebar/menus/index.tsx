import { faClipboardList, faHome, faShoppingCart, IconDefinition } from "@fortawesome/free-solid-svg-icons"

export const Menu: Array<{ icone: IconDefinition, titulo: string, rota: string, dropdown: Array<{ icone: IconDefinition, titulo: string, rota: string }> }> = [
    {
        icone: faHome,
        titulo: "Dashboard",
        rota: 'dashboard',
        dropdown: [],
    },
    {
        icone: faShoppingCart,
        titulo: "Produtos",
        rota: '',
        dropdown: [
            {

                icone: faClipboardList,
                titulo: "Registros",
                rota: '/registros'
            }
        ]
    },
]