import { IconProp } from "@fortawesome/fontawesome-svg-core";
import { faChevronDown, faChevronUp } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { useState } from "react";
import { useHistory } from "react-router-dom";
import { Menu } from "./menus";

export const Sidebar = () => {
    const [dropdownMenu, setDropdownMenu] = useState(true);
    const history = useHistory();

    return (
        <div className="sidebar w-72 h-full bg-primary mt-20 absolute text-xl select-none">
            <ul className="divide-y divide-gray-700">
                {Menu.map((item) =>
                    <div key={item.titulo}>

                        <li className="py-7 h-20 text-white space-x-20 cursor-pointer" onClick={() => item.rota.length > 1 ? history.push(item.rota) : setDropdownMenu(!dropdownMenu)}>
                            <span className='ml-10'>
                                <FontAwesomeIcon className=" mr-4" icon={item.icone} />
                                {item.titulo}
                            </span>
                            {/* 
                              * Condicionais para renderização de icone para dropdown
                            */}
                            {/*  
                              * Primeira condição verifica se existe itens para formar
                              * um dropdown no menu
                            */}
                            {item.dropdown.length > 0 ?
                                /*  
                                 * Segunda Condição verifica se o botão foi clicado
                                 */
                                dropdownMenu ?
                                    <FontAwesomeIcon className='ml-10 mr-4' icon={faChevronDown} />
                                    : <FontAwesomeIcon className='ml-10 mr-4' icon={faChevronUp} />
                                // Final da Segunda Condição
                                : ''
                                // Final da Primeira condição
                            }
                        </li>
                        {item.dropdown.map((dropdown, index) =>
                            dropdownMenu ?
                                <li key={index} className="py-7 h-20 text-white bg-dropdown cursor-pointer" onClick={() => history.push(dropdown.rota)}>
                                    <span className="ml-12">
                                        <FontAwesomeIcon className="mr-4" icon={dropdown.icone} />
                                        {dropdown.titulo}
                                    </span>
                                </li>
                                : <div className="hidden"></div>
                        )}
                    </div>
                )

                }
            </ul>
        </div>
    )
}