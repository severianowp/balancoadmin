import React from "react";
import { Sidebar } from "./sidebar";
import { Header } from "./header";
import { useLocation } from "react-router-dom"

export const Layout: React.FC = ({ children }) => {

    let pagina = useLocation().pathname;
    const paginaTitulo = pagina.substring(1);
    return (
        <div>
            <div className="layout">
                <Header />
                <Sidebar />

            </div>
            <div className="container relative left-72 top-20">
                <div className="titulo relative text-5xl left-10 top-10 capitalize">
                    {paginaTitulo}
                </div>
                <div className="conteudo">

                    {children}
                </div>
            </div>
        </div>

    )

}