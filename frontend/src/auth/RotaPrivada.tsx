import React from "react";
import { Redirect, Route } from "react-router-dom"

interface RotaTypes {
    autenticado: boolean,
    component: React.FC,
    path: string
}

export const RotaPrivada = ({ autenticado: localData, component: Component, path }: RotaTypes) => {
    return (
        <Route path={path} render={props => {
            if (localData) {
                return <Component />;
            } else {
                return (
                    <Redirect to={{ pathname: "/login", state: { from: props.location } }} />
                )
            }
        }} />
    )
}