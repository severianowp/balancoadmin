import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import reportWebVitals from './reportWebVitals';
import { BrowserRouter } from "react-router-dom";
import { Routes } from './routes';
import { ReactProvider } from './provider/reactContext';
import { AuthProvider } from './provider/authContex';

ReactDOM.render(

  <BrowserRouter>
    <ReactProvider>
      <AuthProvider>
        <Routes />
      </AuthProvider>
    </ReactProvider>
  </BrowserRouter>,
  document.getElementById('root')
);

reportWebVitals();
