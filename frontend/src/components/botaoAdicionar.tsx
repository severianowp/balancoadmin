import React, { ReactNode, useState } from "react";
import { Modal } from "./compartilhados/modal";

export const BotaoAdicionar: React.FC = () => {

    const [modal, mostrarModal] = useState(false);

    return (
        <div>

            <div onClick={() => mostrarModal(!modal)} className="adicionar w-60 rounded-lg shadow-md bg-secondary text-center py-4 text-2xl font-medium text-white cursor-pointer select-none">
                Adicionar
        </div>
            {modal ?
                <Modal mostrarModal={mostrarModal} />
                : ''}


        </div>
    )
}