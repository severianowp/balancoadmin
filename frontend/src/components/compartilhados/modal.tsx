import { Dispatch, ReactNode, SetStateAction, useState } from "react"

interface propsTypes {
    children?: ReactNode;
    mostrarModal: Dispatch<SetStateAction<boolean>>
}

export const Modal: React.FC<propsTypes> = ({ mostrarModal }) => {

    return (
        <div className="fixed z-10 inset-0 overflow-y-auto">
            <div className="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
                <div className="fixed inset-0 transition-opacity" aria-hidden="true">
                    <div onClick={() => mostrarModal(false)} className="absolute inset-0 bg-gray-500 opacity-75"></div>
                </div>
                <span className="hidden sm:inline-block sm:align-middle sm:h-screen" aria-hidden="true">&#8203;</span>
                <div className="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-lg sm:w-full lg:w-5/12" role="dialog" aria-modal="true" aria-labelledby="modal-headline">
                    <div className="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                        <div className="sm:flex sm:items-start">
                            <div className="mt-3 lg:text-center sm:mt-0 sm:ml-4 sm:text-left">
                                <h3 className="text-xl leading-6 font-medium text-gray-900">
                                    Adicionar produto
            </h3>
                                <div className="modal-conteudo mx-auto flex flex-wrap px-20">
                                    <div className="titulo ml-4 mr-8 my-4">
                                        <label className="block text-left text-xl font-medium">Titulo:</label>
                                        <input type="text" className="w-30 h-8 bg-white border border-gray-400" />
                                    </div>
                                    <div className="quantidade ml-8 my-4">
                                        <label className="block text-left text-xl font-medium">Quantidade:</label>
                                        <input type="number" className="w-30 h-8 bg-white border border-gray-400" />
                                    </div>
                                    <div className="Valor ml-4 mr-8 my-4">
                                        <label className="block text-left text-xl font-medium">Valor unitário:</label>

                                        <input type="number" className="w-30 h-8 bg-white border border-gray-400" />
                                    </div>
                                    <div className="titulo ml-8 my-4">
                                        <label className="block text-left text-xl font-medium">Data:</label>

                                        <input type="text" className="w-30 h-8 bg-white border border-gray-400" />
                                    </div>

                                </div>
                                <div className="botao mx-auto mt-10 w-3/12 outline-none">
                                    <div onClick={() => mostrarModal(false)} className="bg-primary text-white py-2 px-3 text-center rounded-md font-medium select-none cursor-pointer">
                                        ADICIONAR</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    )
}