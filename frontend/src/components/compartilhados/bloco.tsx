import React, { CSSProperties, ReactNode } from "react";

interface propsTypes {
    texto?: string;
    children?: ReactNode;
}

export const Bloco: React.FC<propsTypes> = ({ texto }) => {
    const meuCSS: CSSProperties = {
        justifyContent: 'space-between',
    }

    return (
        <div className="container-registros h-full w-full rounded-md border border-gray-400 shadow-lg">
            <div className="bg-primary h-10 pt-1 text-xl font-medium text-center text-white">
                <span>
                    {texto}
                </span>
            </div>
            <div className="conteudo px-8">
                <div className="header flex text-lg font-medium mt-2">
                    <span>Mês de Referência</span>
                    <span>Valor</span>
                </div>
                <ul className="divide-y divide-gray-400">
                    <li className="flex py-2" style={meuCSS}>
                        <span>
                            Set
                        </span>
                        <span>
                            R$ 2000.00
                        </span>
                    </li>
                    <li className="flex py-2" style={meuCSS}>
                        <span>
                            Out
                        </span>
                        <span>
                            R$ 100.00
                        </span>
                    </li>
                    <li className="flex py-2" style={meuCSS}>
                        <span>
                            Nov
                        </span>
                        <span>
                            R$ 505.00
                        </span>
                    </li>
                    <li className="flex py-2" style={meuCSS}>
                        <span>
                            Dez
                        </span>
                        <span>
                            R$ 1232.00
                        </span>
                    </li>

                </ul>
            </div>
        </div>
    )
}