module.exports = {
  purge: ['./src/**/*.{js,jsx,ts,tsx}', './public/index.html'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    textColor: {
        'primary': '#181818',
        'secondary': '#543B11',
        'white': "#fafafa"
    },
    backgroundColor: theme => ({
      ...theme("colors"),
      'primary': "#113353",
      'secondary': "#DA8D0D",
      "lightgray": "#fafafa",
      "dropdown": "#1A446C"
    }),
    minWidth: {
      '0': '0',
      '100': '100px',
      '200': '200px',
      '300': '300px',
      '400': '400px',
      '500': '500px'
     }
  },
  variants: {
    extend: {},
  },
  plugins: [],
  corePlugins: {
   maxWidth: false,
  }
}
