const firebase = require ("../../config/firebase");
const usuario = require("../../models/usuario");


module.exports = {
    getData: async (req) => {
        try {
            /**
             * função where(campo, operador, valor)
             * Obs: O campo 'operador' da função where não aceita os operadores: &&, ||, !=
             */
            let usuarioInfo = [];
            const db = firebase.collection('usuarios');
            await db.where("senha", "==", req.senha).get()
            .then(snapshot => {
                snapshot.forEach(doc => usuarioInfo = doc.data());
            })
            return usuarioInfo;
        } catch (error) {
            return false
        }
    }
}