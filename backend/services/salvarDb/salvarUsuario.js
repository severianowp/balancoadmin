const firebase = require("../../config/firebase");
const usuario = require("../../models/usuario");

module.exports = {
    saveData: async (req) => {  
            /**
             * Função que cria o usuário no banco de dados se for único;
             */
            try {

                const salvarDb = firebase.collection("usuarios");
    
                await salvarDb.doc(req.email).set(usuario.usuarioModel(req.nome, req.email, req.senha)
                )
                return true
            } catch(err) {
                return false
            }

        }
    }