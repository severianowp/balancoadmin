const firebase = require("../../config/firebase");
const listarDados = require("../listarDados/listarDados");
const salvarProduto = require("../salvarDb/salvarProduto");

module.exports = {
    checkProdutos: async(req) => {
        /**
         * checagem para ver se a requisição
         * está enviando o email corretamente;
         */
            let usuarioExiste = false;
 
            /**
             * A função faz uma consulta no banco de dados
             * para poder retornar um array com os produtos
             */

             /**
             * função where(campo, operador, valor)
             * Obs: O campo 'operador' da função where não 
             * aceita os operadores: &&, ||, !=
             */

            const busca = firebase.collection('usuarios');
             const buscaEmail = await busca.where("email", "==", req).get();
             const email = buscaEmail.docs.forEach(usuario => usuarioExiste = usuario.exists ? true : false);

            if (usuarioExiste) {
                /**
                 * Após a checagem, irá retornar um objeto
                 * contendo as informações do banco de dados
                 */
                const salvarDb = await salvarProduto.saveProduto(req);
                return salvarDb;
            }

            return false;
    }
}