const firebase = require("../../config/firebase");
const salvarDado = require("../salvarDb/salvarUsuario");
const listarDados = require("../listarDados/listarDados");

module.exports = {
    checkData: async(req) => {
        try {
            /**
             * Antes de inserir no banco de dados, há uma
             *  verificação se o dado já existe
             */
            
            let cadastroExiste = false;
            /**
             * funçãoWhere(campo, operador, valor)
             * Obs: O campo 'operador' da função where não
             *  aceita os operadores: &&, ||, !=
             */
                 const busca = await firebase.collection('usuarios').where("email", "==", req.params.email).get();
                 const filtro = await busca.docs.forEach(item => cadastroExiste = item.exists ? true : false);

             // Depois de verificar se é único, irá salvar as informações no banco de dados
            if (!cadastroExiste) {
                const salvarDb = await salvarDado.saveData(req.params);
                return salvarDb;
            } 
            
            return false;

        } catch (err) {
            console.log(err)
        }
    }
}