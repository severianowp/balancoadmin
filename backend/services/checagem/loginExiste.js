const firebase = require("../../config/firebase");
const listarDados = require("../listarDados/listarDados");

module.exports = {
    checkLogin: async(req) => {

            let loginExiste = false;

            /**
             * A função faz uma consulta no banco de dados
             * e retorna se existe um documento que contenha
             * exatamente o email e a senha informados no login
             */

             /**
             * função where(campo, operador, valor)
             * Obs: O campo 'operador' da função where não 
             * aceita os operadores: &&, ||, !=
             */

            const busca = firebase.collection('usuarios');
             const buscaEmail = await busca.where("email", "==", req.email).where("senha", "==", req.senha).get();
             const email = buscaEmail.docs.forEach(email => loginExiste = email.exists ? true : false);

            if (loginExiste) {
                /**
                 * Após a checagem, irá retornar um objeto
                 * contendo as informações do banco de dados
                 */
                const listaDocumento = await listarDados.getData(req);
                return listaDocumento;
            }

            return false;
    }
}