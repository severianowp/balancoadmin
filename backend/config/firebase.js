const admin = require("firebase-admin");
const serviceAccount = require("../resource/key.json");

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount)
})
const app = admin.firestore();

module.exports = app;