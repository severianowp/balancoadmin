const usuarioPost = require("../services/checagem/cadastroExiste");

module.exports = app => {
    app.post('/cadastro', async (req, res) => 
    {
        try {
            const data = await usuarioPost.checkData(req.body);
            if (data) {
                res.status(201).json({
                    status: 201,
                    message: "Usuário criado com sucesso!",
                });
                return;
            }

            res.status(200).json({
                status: 200,
                message: "Usuário já existe!"}
                );

        } catch(err) {
            res.status(500).json({
                status: 500,
                message: err
            })
        }
});
};