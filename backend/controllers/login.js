const checarLogin = require("../services/checagem/loginExiste");

module.exports = app => {
    app.get('/login', async (req, res) => {
        try {
            const data = await checarLogin.checkLogin(req.query);
            if (data) {
                res.status(200).json({
                    status: 200,
                    message: "Usuário localizado!",
                    data: data,
                });
                return;
            }

            res.status(204).json({
                status: 204,
                message: "Usuário não localizado",
            })

        } catch(err) {
            res.status(500).json({
                status: 500,
                message: err
            })
        }
    });
    
}