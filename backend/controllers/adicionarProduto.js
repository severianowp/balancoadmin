const usuarioPost = require("../services/checagem/cadastroExiste");
const usuarioProdutos = require("../services/checagem/usuarioProdutos");

module.exports = app => {
    app.post('/produto', async (req, res) => 
    {
        try {
            const data = await usuarioProdutos.checkProdutos(req.body.email);
                res.status(201).json({
                    status: 201,
                    message: "Produto Inserido",
                });
                return;
            

        } catch(err) {
            res.status(500).json({
                status: 500,
                message: err
            })
        }
});
};