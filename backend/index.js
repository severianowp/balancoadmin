const customExpress = require("./config/customExpress");
const PORT = 3001;
const app = customExpress();

app.listen(PORT, () => console.log("\n Servidor rodando na porta => " + PORT + "\n"));